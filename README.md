# Archivos: RDF/XML, JSON-LD y SHALCex; y Consultas SPARQL #

Este repositorio contiene archivos RDF/XML, JSON-LD y SHALCex generados a partir de microdata. Además de las consultas SPARQL.

### Listado de archivos ###

* RDF/XML
* JSON-LD
* SHALCex
* SPARQL

